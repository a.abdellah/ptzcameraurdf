<?xml version="1.0"?>
<!-- Revolute-Revolute Manipulator -->
<robot name="ptzcamera" xmlns:xacro="http://www.ros.org/wiki/xacro">

  <!-- Constants for robot dimensions -->
  <xacro:property name="PI" value="3.1415926535897931"/>

  <xacro:property name="mass1" value="0.2" /> <!-- mass1 = masse  -->
  <xacro:property name="radius" value="0.1"/>

  <xacro:property name="compositesBistableWidth" value="0.2"/><!-- link1 is composites bistables -->

  <xacro:property name="supportLength" value="0.05"/>
  <xacro:property name="supportWidth" value="0.015"/> 
  <xacro:property name="supportMass" value="0.1"/>

  <xacro:property name="support1Height" value="0.11"/><!-- support1Height = radius -->
  <xacro:property name="support2Height" value="0.095"/>

  <xacro:property name="supportCameraWidth" value="0.095"/>
  <xacro:property name="supportCameraRadius" value="0.01"/>
  <xacro:property name="supportCameraMass" value="0.02" />

  <xacro:property name="cameraChassisWidth" value="0.015"/><!--0.015 -->
  <xacro:property name="cameraChassisRadius" value="0.08"/>
  <xacro:property name="cameraChassisMass" value="0.05" />

  <xacro:property name="sphereRadius" value="0.025"/>
  <xacro:property name="sphereMass" value="0.03"/>

  <xacro:property name="cameraRadius" value="0.015"/>

  <!-- Import all Gazebo-customization elements, including Gazebo colors -->
  <xacro:include filename="$(find ptzcamera_description)/urdf/ptzcamera.gazebo" />
  <!-- Import Rviz colors -->
  <xacro:include filename="$(find ptzcamera_description)/urdf/materials.xacro" />

  <macro name="cylinder_inertia" params="m r h">
     <inertia  ixx="${m*(3*r*r+h*h)/12}" ixy = "0" ixz = "0"
                  iyy="${m*(3*r*r+h*h)/12}" iyz = "0"
                  izz="${m*r*r/2}" /> 
  </macro>

  <macro name="box_inertia" params="m x y z">
     <inertia  ixx="${m*(y*y+z*z)/12}" ixy = "0" ixz = "0"
                  iyy="${m*(x*x+z*z)/12}" iyz = "0"
                  izz="${m*(x*x+z*z)/12}" /> 
  </macro>

  <macro name="sphere_inertia" params="m r">
      <inertia  ixx="${2*m*r*r/5}" ixy = "0" ixz = "0"
        iyy="${2*m*r*r/5}" iyz = "0"
        izz="${2*m*r*r/5}"
      />
  </macro>


  <!-- Used for fixing robot to Gazebo 'base_link' -->
  <link name="world"/>

  <joint name="fixed" type="fixed">
    <parent link="world"/>
    <child link="compositesBistable"/>
  </joint>

  <link name="compositesBistable">
        <collision>
          <origin xyz="0 0 ${compositesBistableWidth/2}" rpy="0 0 0"/>
          <geometry>
           <cylinder length="${compositesBistableWidth}" radius="${radius}"/>
          </geometry>
        </collision>

        <visual>
          <origin xyz="0 0 ${compositesBistableWidth/2}" rpy="0 0 0"/>
          <geometry>
           <cylinder length="${compositesBistableWidth}" radius="${radius}"/>
          </geometry>
          <material name="green"/>
        </visual>

        <inertial>
          <origin xyz="0 0 ${compositesBistableWidth/2}" rpy="0 0 0"/>
          <mass value="${mass1}"/>
          <cylinder_inertia m="${mass1}" r="${radius}" h="${compositesBistableWidth}"/>
        </inertial>
   </link>



     <link name="cameraSupport">
        <collision>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
           <cylinder length="${supportCameraWidth}" radius="${supportCameraRadius}"/>
          </geometry>
        </collision>

        <visual>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
           <cylinder length="${supportCameraWidth}" radius="${supportCameraRadius}"/>
          </geometry>
          <material name="black"/>
        </visual>

        <inertial>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <mass value="${supportCameraMass}"/>
          <cylinder_inertia m="${supportCameraMass}" r="${supportCameraRadius}" h="${supportCameraWidth}"/>
        </inertial>
    </link>

       <joint name="bearingCameraJoint" type="fixed">
          <parent link="compositesBistable"/>
          <child link="cameraSupport"/>
          <origin xyz="0 0 ${compositesBistableWidth+supportCameraWidth/2}" rpy="0 0 0" /> 
        </joint>

    <link name="cameraChassis">
        <collision>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
           <cylinder length="${cameraChassisWidth}" radius="${cameraChassisRadius}"/>
          </geometry>
        </collision>

        <visual>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
           <cylinder length="${cameraChassisWidth}" radius="${cameraChassisRadius}"/>
          </geometry>
          <material name="white"/>
        </visual>

        <inertial>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <mass value="${cameraChassisMass}"/>
          <cylinder_inertia m="${cameraChassisMass}" r="${cameraChassisRadius}" h="${cameraChassisWidth}"/>
        </inertial>
    </link>

    <joint name="cameraChassisJoint" type="fixed">
      <origin xyz="0 0 ${-cameraChassisWidth/2+support1Height/2}" rpy="0 ${PI/4} 0"/>
      <parent link="cameraSupport"/>
      <child link="cameraChassis"/>
    </joint>

    <link name="fakeLink">
        <collision>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
           <cylinder length="${cameraChassisWidth}" radius="${cameraRadius}"/>
          </geometry>
        </collision>

        <visual>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
          <cylinder length="${cameraChassisWidth}" radius="${cameraRadius}"/>
          </geometry>
          <material name="white"/>
        </visual>

        <inertial>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <mass value="${supportCameraMass/2}"/>
          <cylinder_inertia m="${cameraChassisMass}" r="${cameraRadius}" h="${cameraChassisWidth}"/>
        </inertial>
    </link>

    <joint name="fakeLink" type="fixed">
      <!-- <origin xyz="0 0 ${cameraChassisWidth*1.5}" rpy="0 ${-PI/4} 0"/> -->
      <origin xyz="0 0 ${cameraChassisWidth*1.5}" rpy="0 0 0"/>
      <parent link="cameraChassis"/>
      <child link="fakeLink"/>
    </joint>

     <link name="pan">
        <collision>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
           <cylinder length="${cameraChassisWidth}" radius="${cameraRadius}"/>
          </geometry>
        </collision>

        <visual>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
          <cylinder length="${cameraChassisWidth}" radius="${cameraRadius}"/>
          </geometry>
          <material name="white"/>
        </visual>

        <inertial>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <mass value="${supportCameraMass/2}"/>
          <cylinder_inertia m="${cameraChassisMass}" r="${cameraRadius}" h="${cameraChassisWidth}"/>
        </inertial>
    </link>

    <joint name="panJoint" type="revolute">
      <axis xyz="0 0 1"/>
      <limit effort="1000.0" lower="-3.14159" upper="3.14159" velocity="0.5"/>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <parent link="fakeLink"/>
      <child link="pan"/>
    </joint>


    <link name="tilt">
      <collision>
        <origin xyz="0 0 0" rpy="0 0 0"/>
        <geometry>
          <sphere radius="${sphereRadius}"/>
        </geometry>
      </collision>
        
      <visual>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
            <sphere radius="${sphereRadius}"/>
          </geometry>
          <material name="red"/>
      </visual>

      <inertial>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <mass value="${sphereMass}"/>
          <sphere_inertia m="${sphereMass}" r="${sphereRadius}"/>
      </inertial>
    </link>

    <joint name="tiltJoint" type="revolute">
      <axis xyz="0 1 0"/>
      <limit effort="1000.0" lower="${-PI/2}" upper="0" velocity="0.5"/>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <parent link="pan"/>
      <child link="tilt"/>
    </joint>

    <link name="roll">
      <collision>
        <origin xyz="0 0 0" rpy="0 0 0"/>
        <geometry>
          <sphere radius="${sphereRadius}"/>
        </geometry>
      </collision>
        
      <visual>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <geometry>
            <sphere radius="${sphereRadius}"/>
          </geometry>
          <material name="red"/>
      </visual>

      <inertial>
          <origin xyz="0 0 0" rpy="0 0 0"/>
          <mass value="${sphereMass}"/>
          <sphere_inertia m="${sphereMass}" r="${sphereRadius}"/>
      </inertial>
    </link>

    <joint name="rollJoint" type="revolute">
      <axis xyz="1 0 0"/>
      <limit effort="1000.0" lower="${-PI/2}" upper="${PI/2}" velocity="0.5"/>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <parent link="tilt"/>
      <child link="roll"/>
    </joint>

    <transmission name="tran1">
      <type>transmission_interface/SimpleTransmission</type>
      <joint name="panJoint">
        <hardwareInterface>hardware_interface/PositionJointInterface</hardwareInterface>
      </joint>
      <actuator name="motor1">
        <hardwareInterface>hardware_interface/PositionJointInterface</hardwareInterface>
        <mechanicalReduction>1</mechanicalReduction>
      </actuator>
    </transmission>

    <transmission name="tran2">
      <type>transmission_interface/SimpleTransmission</type>
      <joint name="tiltJoint">
        <hardwareInterface>hardware_interface/PositionJointInterface</hardwareInterface>
      </joint>
      <actuator name="motor1">
        <hardwareInterface>hardware_interface/PositionJointInterface</hardwareInterface>
        <mechanicalReduction>1</mechanicalReduction>
      </actuator>
    </transmission>

    <transmission name="tran3">
      <type>transmission_interface/SimpleTransmission</type>
      <joint name="rollJoint">
        <hardwareInterface>hardware_interface/PositionJointInterface</hardwareInterface>
      </joint>
      <actuator name="motor1">
        <hardwareInterface>hardware_interface/PositionJointInterface</hardwareInterface>
        <mechanicalReduction>1</mechanicalReduction>
      </actuator>
    </transmission>

</robot>
