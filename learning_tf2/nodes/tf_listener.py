#!/usr/bin/env python  
import roslib
roslib.load_manifest('learning_tf2')
import rospy
import math
import tf
import geometry_msgs.msg
from tf.transformations import euler_from_quaternion, quaternion_from_euler

if __name__ == '__main__':
    rospy.init_node('tf_listener')

    listener = tf.TransformListener()

    # rospy.wait_for_service('spawn')
    # spawner = rospy.ServiceProxy('spawn', turtlesim.srv.Spawn)
    # spawner(4, 2, 0, 'turtle2')
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        try:
            (translation,rotation) = listener.lookupTransform('/user', '/tilt', rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        rpy = (roll, pitch, yaw) = euler_from_quaternion (rotation)
        #rospy.loginfo(roll, pitch, yaw)
        print "rpy : %s" % (rpy,)

        

        rate.sleep()